package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private PizzaIngredientFactory factory;

    @Before
    public void setUp(){factory = new DepokPizzaIngredientFactory();}

    @Test
    public void testCreateDough(){
        Dough dough = factory.createDough();
        assertTrue(dough instanceof CheesyBitesDough);
    }

    @Test
    public void testCreateSauce(){
        Sauce sauce = factory.createSauce();
        assertTrue(sauce instanceof LambSauce);
    }

    @Test
    public void testCreateCheese(){
        Cheese cheese = factory.createCheese();
        assertTrue(cheese instanceof BlueCheese);
    }

    @Test
    public void testCreateClam(){
        Clams clams = factory.createClam();
        assertTrue(clams instanceof SteamedClams);
    }

    @Test
    public void testCreateVeggies(){
        Veggies[] veggies = factory.createVeggies();
        assertTrue(veggies[0] instanceof Mushroom);
        assertTrue(veggies[1] instanceof Onion);
        assertTrue(veggies[2] instanceof Pineapple);

    }
}