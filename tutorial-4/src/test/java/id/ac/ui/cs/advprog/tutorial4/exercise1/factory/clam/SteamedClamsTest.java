package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;


import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class SteamedClamsTest {
    private Clams clams;

    @Before
    public void setUp(){clams = new SteamedClams();}

    @Test
    public void testToString(){ assertEquals("Steamed Clams from Principal Skinner",clams.toString());}
}