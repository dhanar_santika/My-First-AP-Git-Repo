package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;


import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class CheesyBitesDoughTest {
    private Dough dough;

    @Before
    public void setUp(){dough = new CheesyBitesDough();}

    @Test
    public void testToString(){ assertEquals("Cheesy Bites Dough",dough.toString());}
}