package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;


import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class BlueCheeseTest {
    private Cheese cheese;

    @Before
    public void setUp(){cheese = new BlueCheese();}

    @Test
    public void testToString(){ assertEquals("Blue Cheese",cheese.toString());}
}
