package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CheesyBitesDough implements Dough{
    public String toString() {
        return "Cheesy Bites Dough";
    }
}
