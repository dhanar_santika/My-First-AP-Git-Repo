package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SteamedClams implements Clams {
    public String toString() {
        return "Steamed Clams from Principal Skinner";
    }
}
