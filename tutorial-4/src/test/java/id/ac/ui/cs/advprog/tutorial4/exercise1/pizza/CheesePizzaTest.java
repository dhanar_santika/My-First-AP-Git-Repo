package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;


import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
    private PizzaIngredientFactory factory;
    private Pizza pizza;

    @Before
    public void setUp(){
        // for test purpose
        factory = new DepokPizzaIngredientFactory();
        pizza = new CheesePizza(factory);
        pizza.setName("Depok Style Cheese Pizza");
    }

    @Test
    public void testName(){
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void testPrepare(){
        pizza.prepare();
    }
}