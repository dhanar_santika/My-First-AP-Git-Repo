package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class LambSauce implements Sauce {
    public String toString() {
        return "Gordon Ramsay's Lamb Sauce";
    }
}
