package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class CompanyTestDrive {
    public static void main(String[] args){
        Company company = new Company();

        Employees luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Employees zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        Employees franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        Employees usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);

        Employees nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);

        Employees robin = new FrontendProgrammer("Robin", 130000.00);
        company.addEmployee(robin);

        Employees sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        Employees brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        Employees chopper = new SecurityExpert("Chopper", 80000.00);
        company.addEmployee(chopper);

        System.out.println("Employees : " + company.getAllEmployees());
        System.out.println("Net Salaries : " + company.getNetSalaries());
    }
}
