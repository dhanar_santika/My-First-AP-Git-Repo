package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;


import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class LambSauceTest {
    private Sauce sauce;

    @Before
    public void setUp(){sauce = new LambSauce();}

    @Test
    public void testToString(){ assertEquals("Gordon Ramsay's Lamb Sauce",sauce.toString());}
}