package id.ac.ui.cs.advprog.tutorial4.exercise1;


import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NewYorkPizzaStoreTest {
    private PizzaIngredientFactory factory;
    private PizzaStore pizzaStore;

    @Before
    public void setUp(){
        // for test purpose
        factory = new NewYorkPizzaIngredientFactory();
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreateCheesePizza(){
        assertEquals("New York Style Cheese Pizza", pizzaStore.createPizza("cheese").getName());
    }

    @Test
    public void testCreateVeggiePizza(){
        assertEquals("New York Style Veggie Pizza", pizzaStore.createPizza("veggie").getName());
    }

    @Test
    public void testCreateClamPizza(){
        assertEquals("New York Style Clam Pizza", pizzaStore.createPizza("clam").getName());
    }

    @Test
    public void testCreateNullPizza(){
        assertTrue(pizzaStore.createPizza("")==null);
    }
}