package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BlueCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SteamedClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheesyBitesDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.LambSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new CheesyBitesDough();
    }

    @Override
    public Sauce createSauce() {
        return new LambSauce();
    }

    @Override
    public Cheese createCheese() {
        return new BlueCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Mushroom(), new Onion(), new Pineapple()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new SteamedClams();
    }
}
