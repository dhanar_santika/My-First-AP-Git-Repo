package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class DecoratorTestDrive {
    public static void main(String[] args){
        Food burger = new ThickBunBurger();

        burger = new BeefMeat(burger);
        burger = new Cheese(burger);
        burger = new Lettuce(burger);
        burger = new Cucumber(burger);
        burger = new ChiliSauce(burger);

        System.out.println("Food : "+burger.getDescription());
        System.out.println("Cost : "+burger.cost());
    }
}
